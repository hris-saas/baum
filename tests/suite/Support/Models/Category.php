<?php

namespace HRis\Baum\Tests\Support\Models;

use Illuminate\Database\Eloquent\Model;
use HRis\Baum\NestedSet\Node;

class Category extends Model
{
    use Node;

    protected $table = 'categories';

    protected $fillable = ['name'];

    public $timestamps = false;
}
