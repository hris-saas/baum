<?php

namespace HRis\Baum\Tests\Support\Models;

class OrderedCluster extends Cluster
{
    protected $orderColumnName = 'name';
}
