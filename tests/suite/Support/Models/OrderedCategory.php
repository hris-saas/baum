<?php

namespace HRis\Baum\Tests\Support\Models;

class OrderedCategory extends Category
{
    protected $orderColumnName = 'name';
}
